import { types } from "./actions";

const initialState = { invoices: [], activeInvoiceId: -1 };

export default function todos(state = initialState, action) {
  switch (action.type) {
    case types.ADD_INVOICE:
      return { ...state, invoices: [...state.invoices, action.payload] };
    case types.TOGGLE_COMPLETED:
      const invoiceIndex = state.invoices.findIndex(
        (invoice) => invoice.id === action.invoice.id
      );
      const newCompletedStatus = !state.invoices[invoiceIndex].isCompleted;
      const updatedInvoice = [...state.invoices];
      updatedInvoice[invoiceIndex] = {
        ...state.invoices[invoiceIndex],
        isCompleted: newCompletedStatus,
      };
      return {
        ...state,
        invoices: updatedInvoice,
      };
    default:
      return state;
  }
}
