export const types = {
  ADD_INVOICE: "ADD_INVOICE",
  TOGGLE_COMPLETED: "TOGGLE_COMPLETED",
};

let nextId = 10;
export function addInvoice(invoice) {
  return {
    type: types.ADD_INVOICE,
    payload: { ...invoice, id: String(++nextId) },
  };
}

export function toggleCompletedInvoice(id) {
  return {
    type: types.TOGGLE_COMPLETED,
    invoice: id,
  };
}
