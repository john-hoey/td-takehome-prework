import React, { Fragment, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";

export function InvoiceList() {
  const invoices = useSelector((state) => state.invoices);
  const [total, setTotal] = useState(0);
  // const [incompleteList, setIncompleteList] = useState([]);

  useEffect(() => {
    const incomplete = [];
    invoices.map((invoice) => {
      if (invoice.isCompleted === false) {
        incomplete.push(invoice);
      }
      // setIncompleteList(incomplete);

      return setTotal(incomplete.length);
    });
  }, [invoices]);

  return (
    <Fragment>
      <ul className="InvoiceList nav nav-pills flex-column">
        {invoices.map((invoice) => (
          <li className="nav-item" key={invoice.id}>
            <NavLink to={`/invoices/${invoice.id}`} className="nav-link">
              #{invoice.id} {invoice.client}
            </NavLink>
          </li>
        ))}
      </ul>
      <p
        style={{
          margin: "3px 0px",
        }}
      >
        <span
          style={{
            backgroundColor: "#007bff",
            color: "white",
            fontSize: "1rem",
            padding: "1px 10px 3px 10px",
            borderRadius: "50%",
          }}
        >
          {total}
        </span>
        <span style={{ marginLeft: "10px" }}>Incomplete</span>
      </p>
    </Fragment>
  );
}
