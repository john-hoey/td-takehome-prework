import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { InvoiceSection } from "./InvoiceSection";
import { toggleCompletedInvoice } from "../actions";
import "./Invoice.css";

export function Invoice() {
  const { id } = useParams();
  const invoice = useSelector(({ invoices }) =>
    invoices.find((invoice) => invoice.id === id)
  );

  const dispatch = useDispatch();
  //invoiceTotal = 0 (initial state)
  const [invoiceTotal, setInvoiceTotal] = useState(0);

  const isCompletedToggleHander = (e) => {
    e.preventDefault();
    dispatch(toggleCompletedInvoice({ id }));
  };

  //useEffect hook to run after initial page render
  useEffect(() => {
    let subtotals = []; //container for subtotal values
    let projectTotal = invoice.projects; //access project data by id (thanks to useParams/useSelector) returns an array
    let servicesTotal = invoice.services; //access services data. returns an array
    let sum = 0;

    //push value for each project and service item into subtotal array, use conditionals to keep new invoices from error'ing out from lack of projects and services.
    if (projectTotal) {
      projectTotal.map((project) => {
        return subtotals.push(
          ((project.price_cents * project.quantity) / 100) *
            ((100 - project.discount_percent) / 100) // discount math
        );
      });
    } else {
      return;
    }
    if (servicesTotal) {
      servicesTotal.map((service) => {
        return subtotals.push(
          ((service.price_cents * service.quantity) / 100) *
            ((100 - service.discount_percent) / 100) // discount math.
        );
      });
    } else {
      return;
    }
    //add up values in subtotals array
    sum = subtotals.reduce((accumulator, currentValue) => {
      return accumulator + currentValue;
    }, 0);
    setInvoiceTotal(sum); // set invoiceTotal to the sum of all subtotals.
  }, [invoice.projects, invoice.services]); // dependencies for useEffect

  if (!invoice) {
    return (
      <div className="Invoice">
        <h3>Invoice {id} not found.</h3>
      </div>
    );
  }

  return (
    <div className="Invoice">
      <h2>{invoice.client} Invoice</h2>
      <div>
        <strong>Attn: </strong>
        {invoice.attn}
      </div>
      <div>
        <strong>Due Date: </strong>
        {invoice.due_date}
      </div>
      <div>
        <strong>Notes: </strong>
        {invoice.notes}
      </div>
      <br />
      <InvoiceSection title="Projects" items={invoice.projects} />
      <br />
      <InvoiceSection title="Services" items={invoice.services} />
      <div data-testid={`invoice-${invoice.id}-total`}>
        <strong>Total: </strong>${invoiceTotal}
      </div>
      <button
        className={!invoice.isCompleted ? "btn complete" : "btn incomplete"}
        onClick={isCompletedToggleHander}
      >
        {!invoice.isCompleted ? "Mark as complete" : "Mark as incomplete"}
      </button>
    </div>
  );
}
